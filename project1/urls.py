"""project1 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin
from register import views

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    # url(r'^register/', include('register.urls'))
    url(r'^register/$',views.register_fun, name='register'),
    #url(r'^login$',views.login_fun, name='login'),
     url(r'^$',views.login_fun, name='login'),
    # url(r'^chat$',views.chat_fun, name='chat'),
    url(r'^logout/$',views.logout_fun, name='logout'),
    # url(r'^dashboard/$',views.dasboard_fun, name='dashboard'),
    url(r'^dashboard/$',views.dashboard_fun, name='dashboard'),
    url(r'^online/$', views.online, name='online'),
    url(r'^chat/$', views.chat, name='chat'),
    url(r'^messages/$', views.messages, name='messages'),
]
